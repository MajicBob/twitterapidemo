<?php
/**
 * Coded Design Code Challange - List hashtags from CNN twitter feed by time
 *
 * TASK:
 *   1. Pull all hashtags for the current day from https://twitter.com/cnn
 *   2. List the hashtags in order by time
 *
 * RULES:
 *   - Hashtags are alphanumeric strings ([a-zA-Z0-9]) prefixed with the hash symbol "#"
 *     (e.g. "#keyword" is a valid hashtag and #2Words!? counts as "2Words" only)
 *   - Hashtags cannot be in the middle of a words or characters
 *     (e.g. "some#word" and ".?!#word" are invalid)
 *   - Hash symbols ("#") alone are not valid
 *   - If a word is preceded by more than one hash symbol, only one hashtag counts
 *     (e.g. "###keyword" counts only as "keyword")
 *   - Since hashtags are only alphanumeric [a-zA-Z0-9] in case (e.g. "###keyword" counts only as "keyword")
 *   - There should be no matching keyword's in the list
 *
 * @author   John Tribolet <john@tribolet.info>
 */

require './vendor/autoload.php';

// Twitter Config
$twitterApiConfig = array(
    'oauth_access_token'        => '725780536975851520-fEvjJfmdwPgHLqrePz2PY9Vaf5yTocv',
    'oauth_access_token_secret' => 'KqEo93ZeYMD0Tcu4lTkD550Uw0LAgov7w1iXess39KBm4',
    'consumer_key'              => 'fXRgrQFT2IRybjURu0reF05Ri',
    'consumer_secret'           => 'JnBOTv9GczEH5tUOXaEqwuVw1UZjNTAH0nETlAinnHeNl4aRJL',
    'pageSize'                  => 10,
    'screen_name'               => 'CNN'
);


$today = new DateTime();
$today->setTime(0, 0, 0);

$tweetsWithHashTags = [];

// User Timeline method (search method results were not matching web search when filtered for today)
$url      = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
$qs       = '?trim_user=true&count=' . $twitterApiConfig['pageSize'];
$qs      .= '&screen_name=' . $twitterApiConfig['screen_name'];
$method   = 'GET';
$twitter  = new TwitterAPIExchange($twitterApiConfig);

$isToday  = true;
$maxId    = null;
// API returns $data back sorted newest first
while ($isToday) {

    if (!is_null($maxId)) {
        // @todo Append works but would be cleaner to replace
        $qs .= "&max_id=$maxId";
    }
    $resp = $twitter->setGetfield($qs)
        ->buildOauth($url, $method)
        ->performRequest();
    $data = json_decode($resp);

    foreach ($data as $i => $tweet) {
        // created_at is returned in UTC, convert to local for date compare
        $created = new DateTime($tweet->created_at);
        $created->setTimezone(new DateTimeZone(date_default_timezone_get()));

        if ($created < $today) {
            $isToday = false;
            break;
        }

        echo $created->format('Y-m-d H:i:s') . "  -  " . $tweet->text . "\n";

        // Twitter Hashtag data seems to match rules, no need to parse tweet text
        if (count($tweet->entities->hashtags)) {
            $tweet->created_at_local = $created;
            $tweetsWithHashTags[] = $tweet;
        }
    }

    // echo "\n";
    if ($isToday) {
        // Get last Tweet ID and subtract one for max id of next fetch
        $maxId = bcsub($tweet->id_str, '1');
    }
}


// Output Hashtag Info - only output each tag once per rules, use most recent occurence for time
$output = [];
foreach ($tweetsWithHashTags as $tweet) {

    foreach ($tweet->entities->hashtags as $ht) {
        if (!in_array($ht->text, $output)) {
            echo "$ht->text \n";
            $output[] = $ht->text;
        }
    }

}